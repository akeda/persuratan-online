<?php
class GroupsController extends AppController {
    var $pageTitle = 'Groups - Permission Management';
    function add() {
        $this->__setAdditionals();
        $this->set('module_actions', $this->Group->ModuleAction->find('list'));
        parent::add();
    }
    
    function edit($id = null) {
        $this->__setAdditionals(false, $id);
        
        if (!empty($this->data)) {
            $this->Session->delete('Group');    
        }
        
        parent::edit($id);
    }
    
    function __setAdditionals($add = true, $id = null) {
        ClassRegistry::init('Module');
        ClassRegistry::init('MenuType');
        $this->Module = new Module;
        $this->MenuType = new MenuType;
        $modules = $this->Module->find('all', array(
            'fields' => array('Module.id', 'Module.name', 'Module.controller'),
            'order' => 'Module.name ASC'
        ));
        $menus = $this->MenuType->find('all', array(
            'fields' => array('MenuType.id', 'MenuType.name', 'MenuType.call_name'),
            'order' => 'MenuType.name ASC'
        ));
        if (!$add) {
            $action_checked = $this->Group->GroupsModuleAction->find('list', array(
                'conditions' => array('GroupsModuleAction.group_id' => $id),
                'fields' => array('GroupsModuleAction.module_action_id', 'GroupsModuleAction.group_id')
            ));
            $this->set('action_checked', $action_checked);
            foreach ($modules as $i => $module) {
                foreach ( $module['ModuleAction'] as $j => $action ) {
                    if ( isset($action_checked[$action['id']]) ) {
                        $modules[$i]['ModuleAction'][$j]['checked'] = true;
                        $modules[$i]['Module']['checked'] = true;
                    }
                }
            }
            $menu_checked = $this->Group->GroupsMenu->find('list', array(
                'conditions' => array('GroupsMenu.group_id' => $id),
                'fields' => array('GroupsMenu.menu_id', 'GroupsMenu.group_id')
            ));
            $this->set('menu_checked', $menu_checked);
            foreach ($menus as $i => $menu) {
                foreach ( $menu['Menu'] as $j => $m ) {
                    if ( isset($menu_checked[$m['id']]) ) {
                        $menus[$i]['Menu'][$j]['checked'] = true;
                        $menus[$i]['MenuType']['checked'] = true;
                    }
                }
            }
        }
        $this->set('modules', $modules);
        $this->set('menus', $menus);
    }
}
?>