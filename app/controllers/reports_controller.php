<?php
class ReportsController extends AppController {
    var $uses = array();
    var $cacheQueries = true;
    var $pageTitle = 'Report';

    function rekapitulasi() {
        $this->set('maxYear', date('Y'));
        $this->set('minYear', date('Y')-4);
        $this->set('path', "var path = '" . $this->__pathToController() . "';" );
    }

    function arsip() {
        $this->set('maxYear', date('Y'));
        $this->set('minYear', date('Y')-4);
        $this->set('path', "var path = '" . $this->__pathToController() . "';" );
    }

    function letter_ins() {
        $this->set('maxYear', date('Y'));
        $this->set('minYear', date('Y')-4);
        $this->set('path', "var path = '" . $this->__pathToController() . "';" );
    }

    function printhtml() {
        $this->layout = 'printhtml';
        //Configure::write('debug', 0);
        $this->__process();
    }

    function grafik() {
        //Configure::write('debug', 0);
        // $this->autoRender = false;
        $years = array();
        for ( $n = date('Y'); $n >= (date('Y') - 5); $n-- ) {
            $years[$n] = $n;
        }
        $this->set('years', $years);

        // is it by user or unit
        if ( isset($this->data['Report']['by']) ) {
            if ($this->data['Report']['by'] == 'u') { // unit
                if ( $this->data['per'] == '__' ) {
                    $this->data['per'] = null;
                    $subtitle = ' oleh semua unit kerja';
                } else {
                    ClassRegistry::init('UnitCode');
                    $U = new UnitCode;
                    $whom = $U->find('list', array(
                        'conditions' => array(
                            'UnitCode.id' => $this->data['per']
                        ), 'recursive' => -1
                    ));

                    $subtitle = ' oleh unit kerja ' . $whom[ $this->data['per'] ];
                }


                $this->set('subtitle', $subtitle);
            } else { // user
                if ( $this->data['per'] == '__' ) {
                    $this->data['per'] = null;
                    $subtitle = ' oleh semua unit kerja';
                } else {
                    ClassRegistry::init('User');
                    $U = new User;
                    $whom = $U->find('list', array(
                        'conditions' => array(
                            'User.id' => $this->data['per']
                        ), 'recursive' => -1
                    ));
                    $subtitle = ' oleh pengguna ' . $whom[ $this->data['per'] ];
                }
                $this->set('subtitle', $subtitle);
            }

            $this->set('by', $this->data['Report']['by']);
            $this->set('per', $this->data['per']);
        }

        if ( isset($this->data['Report']['years']) ) {
            $y = $this->data['Report']['years'];
        } else {
            $y = date('Y');
        }
        $this->set('y', $y);

        if ( $this->params['url']['m'] == 'rekapitulasi' ) {
            $conditions = array();
            $this->pageTitle = 'Rekapitulasi Surat Keluar Th. ' . date('Y');

            ClassRegistry::init('LetterOut');
            $this->LetterOut = new LetterOut;
            $__letters = $this->LetterOut->getQtyPerMonth($y,
                $this->data['Report']['by'], $this->data['per']);
            $letters = array(
                0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0,
                6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0,
                11 => 0
            );
            foreach ($__letters as $key => $letter) {
                $letters[($letter[0]['m']*1)-1] = $letter[0]['qty'];
            }

            $this->set('m', 'rekapitulasi');
        } else if ( $this->params['url']['m'] == 'letter_ins' ) {
            $this->pageTitle = 'Rekapitulasi Surat Masuk Th. ' . date('Y');

            ClassRegistry::init('LetterIn');
            $this->LetterIn = new LetterIn;
            $__letters = $this->LetterIn->getQtyPerMonth($y,
                $this->data['Report']['by'], $this->data['per']);
            $letters = array(
                0 => 0, 1 => 0, 2 => 0, 3 => 0, 4 => 0, 5 => 0,
                6 => 0, 7 => 0, 8 => 0, 9 => 0, 10 => 0,
                11 => 0
            );
            foreach ($__letters as $key => $letter) {
                $letters[($letter[0]['m']*1)-1] = $letter[0]['qty'];
            }
            $this->set('m', 'letter_ins');
        }
        $this->set('letters', $letters);

        $this->helpers[] = 'chart';

        $options = array();
        $options['type'] = 'bar';
        $options['month'] = true;
        $options['title'] = 'Grafik Kuantitas Surat Th. ' . date('Y') . $subtitle;
        $options['shadow'] = true;
        $options['x'] = 'Bulan';
        $options['y'] = 'Jumlah';
        $options['filename'] = $this->params['url']['m'] . '_' .
            $this->data['Report']['by'] . '_' . $this->data['per'] .
            '_' . $y . '.png';
        $options['stroke'] = false;

        $this->set('options', $options);
        //$this->set('data', $Customers2);
        $letters = array(
            'values' => $letters,
            'name' => 'Bulan'
        );
        $this->set('data', array($letters));
        // $this->set('dataLine', $Customers2);
    }

    function getuser() {
        ClassRegistry::init('User');
        $this->User = new User;
        $users = $this->User->find('list', array(
            'conditions' => array(
                'User.active' => 1
            ),
            'fields' => array('User.id', 'User.name'),
            'recursive' => -1
        ));
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        $this->set('users', $users);
    }

    function getunit() {
        ClassRegistry::init('UnitCode');
        $this->UnitCode = new UnitCode;

        $units = $this->UnitCode->find('list', array(
            'fields' => array('UnitCode.id', 'UnitCode.name'),
            'recursive' => -1
        ));
        $units['__'] = 'All';
        Configure::write('debug', 0);
        $this->layout = 'ajax';

        $this->set('units', $units);
    }

    function __process() {
        if ( !empty($this->data) ) {
            $conditions = array();

            $from = $until = '';
            if ( isset($this->data['year']['year']) && !empty($this->data['year']['year']) ) {
                $from  = $this->data['year']['year'] . '-01-01';
                $until = $this->data['year']['year'] . '-12-31';
            }

            $arsip = false;
            if ( isset($this->data['mode']) && !empty($this->data['mode']) ) {
                $this->params['url']['m'] = $this->data['mode'];
                $verify = 1;
                $arsip = true;
            }
            $this->set('arsip', $arsip);

            $letters = array();
            if ( $this->params['url']['m'] == 'rekapitulasi' ) {
                $this->pageTitle = 'Rekapitulasi Surat Keluar';
                if ( $this->data['Report']['by'] == 'u' ) {
                    // quick hack for all unit code
                    if ( $this->data['per'] != '__' ) {
                        $conditions['LetterOut.unit_code_id'] = $this->data['per'];
                    } else {
                        $this->set('unitAll', 'Semua Unit Kerja');
                    }
                } else if ( $this->data['Report']['by'] == 'p' ) {
                    $conditions['LetterOut.created_by'] = $this->data['per'];
                }

                if ( !empty($from) && !empty($until) ) {
                    $conditions['LetterOut.letter_date >='] = $from;
                    $conditions['LetterOut.letter_date <='] = $until;
                } else {
                    $conditions['LetterOut.letter_date <='] = date('Y-m-d H:i:s');
                }
                if ( isset($verify) ) {
                    $conditions['LetterOut.verified'] = 1;
                }
                ClassRegistry::init('LetterOut');
                $this->LetterOut = new LetterOut;
                $letters = $this->LetterOut->find('all', array(
                    'conditions' => $conditions,
                    'order' => 'YEAR(LetterOut.letter_date) ASC, LetterOut.letter_no ASC, LetterOut.letter_date ASC'
                ));
                $this->set('m', 'rekapitulasi');
            } else if ( $this->params['url']['m'] == 'letter_ins' ) {
                $this->pageTitle = 'Laporan Surat Masuk';
                if ( $this->data['Report']['by'] == 'u' ) {
                    // quick hack for all unit code
                    if ( $this->data['per'] != '__' ) {
                        $conditions['LetterIn.unit_code_id'] = $this->data['per'];
                    } else {
                        $this->set('unitAll', 'Semua Unit Kerja');
                    }
                    //$conditions['LetterIn.unit_code_id'] = $this->data['per'];
                } else if ( $this->data['Report']['by'] == 'p' ) {
                    $conditions['LetterIn.created_by'] = $this->data['per'];
                }
                if ( !empty($from) && !empty($until) ) {
                    $conditions['LetterIn.received_date >='] = $from;
                    $conditions['LetterIn.received_date <='] = $until;
                } else {
                    $conditions['LetterIn.received_date <='] = date('Y-m-d H:i:s');
                }
                if ( isset($verify) ) {
                    $conditions['LetterIn.verified'] = 1;
                }
                ClassRegistry::init('LetterIn');
                $this->LetterIn = new LetterIn;
                $letters = $this->LetterIn->find('all', array(
                    'conditions' => $conditions,
                    'order' => 'LetterIn.received_date ASC, LetterIn.letter_no ASC'
                ));
                $this->set('m', 'letter_ins');
            }
            $this->set('letters', $letters);
            $this->set('per', $this->data['Report']['by']);
        }
    }
}
?>
