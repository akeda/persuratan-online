<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('LetterNumber');?>
	<fieldset>
 		<legend><?php __('Refill Letter No.');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('No. From');?>:</td>
                <td><?php echo $form->input('no_start', array('div'=>false, 'label' => false, 'maxlength' => 10, 'size' => 8, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('No. Until');?>:</td>
                <td><?php echo $form->input('no_end', array('div'=>false, 'label' => false, 'maxlength' => 10, 'size' => 8, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Valid Date From');?>:</td>
                <td><?php echo $form->input('valid_start', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Valid Date Until');?>:</td>
                <td><?php echo $form->input('valid_end', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Active');?>:</td>
                <td><?php echo $form->input('active', array('div'=>false, 'label' => false, 'checked' => 'checked'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>