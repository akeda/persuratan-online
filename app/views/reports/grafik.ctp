<div class="module-head">
    <div class="module-head-c">
        <h2><?php echo isset($this->pageTitle) ? __($this->pageTitle, true) : __($modelName, true); ?></h2>
    </div>
</div>
<?php
    
    echo $form->create('Report', array('action' => 'grafik?m=' . $m, 'id' => 'reportForm'));
    echo '<span class="label">Tahun</span> &nbsp;';
    echo $form->select('years', $years, date('Y'), null, false);
    echo '&nbsp;';
    echo $form->input('by', array('value' => $by, 'type' => 'hidden'));
    echo $form->input('per', array('value' => $per, 'type' => 'hidden', 'name' => 'data[per]'));
    echo $form->submit('Tampilkan', array('div' => false)) . '&nbsp;' . __('or', true) . '&nbsp;';
    echo $html->link(__('Back', true), array('action' => $m));
    echo '</form>';
    
    echo '<div>';
    $chart->setup($data, $options['type'], $options['month'], $options);
    echo $chart->output($options['filename'], false);
    echo '</div>';
?>