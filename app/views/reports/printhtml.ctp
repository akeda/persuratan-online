<style type="text/css">
.center {text-align: center;}
</style>
<?php 
    $_retention = array(
        1 => 'Inaktif 1 tahun',
        2 => 'Inaktif 2 tahun',
        3 => 'Inaktif 3 tahun',
        4 => 'Inaktif 4 tahun',
        5 => 'Inaktif 5 tahun',
        6  => 'Inaktif 6 tahun',
        7  => 'Inaktif 7 tahun',
        8  => 'Inaktif 8 tahun',
        9  => 'Inaktif 9 tahun',
        10 => 'Inaktif 10 tahun',
        15 => 'Inaktif 15 tahun',
        0 => 'Musnah',
        99 => 'Permanen'
    );
    
    if ($m == 'rekapitulasi') {
        if ( $arsip ) {
            echo "<h1>Rekapitulasi Arsip Aktif Surat Keluar</h1>";
            $head = '<tr>
            <th rowspan="2">No</th>
            <th colspan="2" class="center">Arsip</th>
            <th rowspan="2">Tgl Surat</th>
            <th rowspan="2">No Surat</th>
            <th rowspan="2">Hal</th>';
        } else {
            echo "<h1>Rekapitulasi Pengguna Nomor Surat Keluar</h1>";
            $head = '<tr>
            <th rowspan="2">No</th>
            <th rowspan="2">No. Surat</th>
            <th rowspan="2">Tgl Surat</th>
            <th rowspan="2">Ditujukan Kepada</th>
            <th rowspan="2">Kode Hal</th>
            <th rowspan="2">Hal</th>
            <th rowspan="2">Ditandatangani oleh</th>
            <th rowspan="2">Unit Kerja Pemroses</th>
            <th rowspan="2">Nama Pengebon</th>
            <th rowspan="2">Komitmen</th>';
        }
    } else if ($m == 'letter_ins') {
        if ( $arsip ) {
            echo "<h1>Rekapitulasi Arsip Aktif Surat Masuk</h1>";
            $head = '<tr>
            <th rowspan="2">No</th>
            <th colspan="2">Arsip</th>
            <th rowspan="2">Tgl Surat</th>
            <th rowspan="2">No Surat</th>
            <th rowspan="2">Hal</th>';
        } else {
            echo "<h1>Laporan Surat Masuk</h1>";
            $head = '<tr>
            <th rowspan="2">No Agenda</th>
            <th rowspan="2">Tgl. terima surat</th>
            <th rowspan="2">Asal Surat</th>
            <th rowspan="2">No. Surat</th>
            <th rowspan="2">Tgl Surat</th>
            <th rowspan="2">Alamat Pengirim</th>
            <th rowspan="2">Hal</th>
            <th rowspan="2">Di disposisi kpd</th>
            <th rowspan="2">Tgl. disposisi</th>
            <th rowspan="2">Diinput oleh</th>
            <th rowspan="2">Tgl. input</th>
            <th rowspan="2">Unit Kerja Pemroses</th>';
        }
    }
    
    if ( $arsip ) {
        $head .= '<th colspan="'.sizeof($_retention).'" class="center">JADWAL RETENSI / STATUS ARSIP</th>';
        $head .= '<th rowspan="2">No Lemari</th><th rowspan="2">No Box</th>';
        $head .= '<th rowspan="2">No Filling cabine</th><th rowspan="2">No Laci</th>';
        $head .= '<th rowspan="2">Gambar</th>';
        $head .= '</tr>';
        $head .= '<tr>';
        $head .= '<th class="center">Kode</th><th class="center">Masalah</th>';
        foreach ($_retention as $_r => $__r) {
            $head .= '<th>' . $__r . '</th>';
        }
    } else {
        $head .= '<th colspan="'.sizeof($_retention).'" class="center">JADWAL RETENSI / STATUS ARSIP</th>';
        $head .= '<th rowspan="2">Kode Arsip</th><th rowspan="2">No Lemari</th><th rowspan="2">No Box</th>';
        $head .= '<th rowspan="2">No Filling cabine</th><th rowspan="2">No Laci</th>';
        $head .= '<th rowspan="2">Gambar</th>';
        $head .= '</tr>';
        $head .= '<tr>';
        foreach ($_retention as $_r => $__r) {
            $head .= '<th>' . $__r . '</th>';
        }
    }
    $head .= '</tr>';
    
    
    if ( $per == 'u' ) {
        if ( isset($unitAll) ) {
            echo '<span>Unit Kerja: Semua Unit Kerja</span>';
        } else {
            echo '<span>Unit Kerja: ' . $letters[0]['UnitCode']['name'] . '</span>';
        }
    } else if ( $per == 'p' ) {
        echo '<span>Nama: ' . $letters[0]['User']['name'] . '</span>';
    }
    echo '<span>Per tanggal: ' . date('d/m/Y') . '</span>';
?>
<table>
<thead>
    <?php echo $head;?>
</thead>
<?php
foreach ($letters as $key => $letter) {        
    if ($m == 'rekapitulasi') {
        $col3 = $letter['LetterOut']['letter_no'];
        $col4 = $letter['LetterOut']['letter_date'] ? $time->format('d/m/Y', $letter['LetterOut']['letter_date']) : '-';
        $col5 = $letter['LetterOut']['letter_to'];
        $col6 = $letter['TermCode']['name'];
        $col7 = $letter['LetterOut']['terms'];
        $col8 = $letter['LetterOut']['remark'];
        $col9 = $letter['UnitCode']['name'];
        $col10 = $letter['User']['name'];
        $col11 = "Saya telah menyimpan arsip surat ini, dan saya bersedia meminjamkan arsip surat ini bila diperlukan oleh Penata Arsip/Pejabat di Bagian Administrasi Umum dan Keuangan Politeknik Negeri Media Kreatif.";
        
        $code = $letter['TermCode']['code'];
        $cabinet_no = $letter['LetterOut']['cabinet_no'];
        $box_no = $letter['LetterOut']['box_no'];
        $filling_cabinet_no = $letter['LetterOut']['filling_cabinet_no'];
        $filling_box_no = $letter['LetterOut']['filling_box_no'];
        
        $retention = '';
        foreach ($_retention as $_r => $__r) {
            if ( $_r == $letter['LetterOut']['retention'] ) {
                $retention .= '<td class="center">x</td>';
            } else {
                $retention .= '<td>&nbsp;</td>';
            }
        }
        
        $photos = '';
        foreach ($letter['LetterOutImage'] as $kimg => $img) {
            $photos .= $html->link('Gambar ' . ($kimg+1), '/files/letter_out_photos/' . $img['imagename']);
        }
        
    } else {
        $col2 = $letter['LetterIn']['received_date'] ? $time->format('d/m/Y', $letter['LetterIn']['received_date']) : '-';
        $col3 = $letter['LetterIn']['letter_from'];
        $col4 = $letter['LetterIn']['letter_no'];
        $col5 = $letter['LetterIn']['letter_date'] ? $time->format('d/m/Y', $letter['LetterIn']['letter_date']) : '-';
        $col6 = $letter['LetterIn']['letter_from_address'];
        $col7 = $letter['LetterIn']['letter_term'];
        $col8 = $letter['LetterIn']['letter_to'];
        $col9 = $letter['LetterIn']['disposition_date'] ? $time->format('d/m/Y', $letter['LetterIn']['disposition_date']) : '-';
        $col10 = $letter['User']['name'];
        $col11= $time->format('d/m/Y H:i:s', $letter['LetterIn']['created']);
        $col12 = $letter['UnitCode']['name'];
        
        $code = $letter['TermCode']['code'];
        $cabinet_no = $letter['LetterIn']['cabinet_no'];
        $box_no = $letter['LetterIn']['box_no'];
        $filling_cabinet_no = $letter['LetterIn']['filling_cabinet_no'];
        $filling_box_no = $letter['LetterIn']['filling_box_no'];
        
        $retention = '';
        foreach ($_retention as $_r => $__r) {
            if ( $_r == $letter['LetterIn']['retention'] ) {
                $retention .= '<td class="center">x</td>';
            } else {
                $retention .= '<td>&nbsp;</td>';
            }
        }
        
        $photos = '';
        foreach ($letter['LetterInImage'] as $kimg => $img) {
            $photos .= $html->link('Gambar ' . ($kimg+1), '/files/letter_in_photos/' . $img['imagename']) . '<br />';
        }
    }
?>
    <tr>
        <?php if ( $arsip ): ?>
            <td>
            <?php
                if ( $m == 'rekapitulasi' ) {
                    echo $letter['LetterOut']['letter_date'] ? $time->format('d/m/Y', $letter['LetterOut']['letter_date']) : '-';
                } else if ( $m == 'letter_ins' ) {
                    echo $letter['LetterIn']['letter_date'] ? $time->format('d/m/Y', $letter['LetterIn']['letter_date']) : '-';
                }
            ?>
            </td>
            <td><?php echo $letter['TermCode']['code'];?></td>
            <td><?php echo $letter['TermCode']['name'];?></td>
            <td>
            <?php
                if ( $m == 'rekapitulasi' ) {
                    echo $letter['LetterOut']['letter_date'] ? $time->format('d/m/Y', $letter['LetterOut']['letter_date']) : '-';
                } else if ( $m == 'letter_ins' ) {
                    echo $letter['LetterIn']['letter_date'] ? $time->format('d/m/Y', $letter['LetterIn']['letter_date']) : '-';
                }
            ?>
            </td>
            <td>
            <?php
                if ( $m == 'rekapitulasi' ) {
                    echo $letter['LetterOut']['letter_no'];
                } else if ( $m == 'letter_ins' ) {
                    echo $letter['LetterIn']['letter_no'];
                }
            ?>
            </td>
            <td><?php echo $letter['TermCode']['name'];?></td>
            <?php echo $retention;?>
            <td><?php echo $cabinet_no;?></td>
            <td><?php echo $box_no;?></td>
            <td><?php echo $filling_cabinet_no;?></td>
            <td><?php echo $filling_box_no;?></td>
            <td><?php echo $photos;?></td>
        <?php else: ?>
            <td><?php echo $key+1;?></td>
            <?php
            if ($m == 'letter_ins') {
                echo "<td>$col2</td>";
            }
            ?>
            <td><?php echo $col3;?></td>
            <td><?php echo $col4;?></td>
            <td><?php echo $col5;?></td>
            <td><?php echo $col6;?></td>
            <td><?php echo $col7;?></td>
            <td><?php echo $col8;?></td>
            <td><?php echo $col9;?></td>
            <td><?php echo $col10;?></td>
            <td><?php echo $col11;?></td>
            <?php
                if ($m == 'rekapitulasi') {
                } else if ($m == 'letter_ins') {
                    echo "<td>$col12</td>";
                }
            ?>
            <?php echo $retention;?>
            <td><?php echo $code;?></td>
            <td><?php echo $cabinet_no;?></td>
            <td><?php echo $box_no;?></td>
            <td><?php echo $filling_cabinet_no;?></td>
            <td><?php echo $filling_box_no;?></td>
            <td><?php echo $photos;?></td>
        <?php endif; ?>
    </tr>
<?php
}
?>
</table>
