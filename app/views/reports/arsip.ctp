<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('Report', array('action' => 'printhtml', 'id' => 'reportForm'));?>
	<fieldset>
 		<legend><?php __('Rekapitulasi Arsip Aktif');?></legend>
        <table class="input">
            <tr>
                <td class="label-required">Tahun:</td>
                <td>
                <?php
                    echo $form->year('year', $minYear, $maxYear, date('Y'), array(
                        'div' => false
                    ), false);
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Surat Masuk / Keluar:</td>
                <td>
                <?php
                    echo $form->select('mode', array('rekapitulasi' => 'Surat Keluar', 'letter_ins' => 'Surat Masuk'), null,
                        array("class" => "inputText", 'name' => 'data[mode]'), false
                    );
                ?>
                </td>
            </tr>
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Per Pengguna / Unit Kerja');?>:</td>
                <td>
                    <?php
                        $option_by = array(
                            "" => "- Select one -",
                            "p" => __("Pengguna", true), 
                            "u" => __("Unit Kerja", true)
                        );
                        echo $form->select("by", $option_by, null, array("id" => "by", "class" => "inputText"), false);
                    ?>
                </td>
            </tr>
            <tr>
                <td>Pilih Pengguna / Unit Kerja</td>
                <td id="changed"></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print')) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $form->submit(__('Lihat grafik', true), array('div'=>false, 'id' => 'grafik'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php echo $javascript->codeBlock($path); ?>
<?php echo $javascript->link('reports'); ?>
