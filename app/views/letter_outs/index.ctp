<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "letter_no" => array(
                    'title' => __("Letter No.", true), 
                    'sortable' => false
                ),
                "letter_date" => array(
                    'title' => __("Date", true),
                    'sortable' => true
                ),
                "created_by" => array(
                    'title' => __("Di input oleh", true),
                    'sortable' => true
                ),
                "unit_code_id" => array(
                    'title' => __("Unit", true),
                    'sortable' => true
                ),
                "term_code_id" => array(
                    'title' => __("Kode Hal", true),
                    'sortable' => true
                ),
                "terms" => array(
                    'title' => __("Hal", true),
                    'sortable' => false
                ),
                "letter_to" => array(
                    'title' => __("Ditujukan kepada", true),
                    'sortable' => false
                ),
                "remark" => array(
                    'title' => __("Ditandatangani oleh", true),
                    'sortable' => false
                ),
                "created" => array(
                    'title' => __("Waktu input", true),
                    'sortable' => true
                )
              ),
              "editable"  => "letter_no",
              "editableLink"  => "view",
              "assoc" => array(
                'unit_code_id' => array(
                    'field' => 'name',
                    'model' => 'UnitCode'
                ),
                'term_code_id' => array(
                    'field' => 'name',
                    'model' => 'TermCode'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              "replacement" => array(
                'active' => array(
                    1 => array(
                        'replaced' => '<strong class="green">' . __('Yes', true) . '</strong>'
                    ),
                    0 => array(
                        'replaced' => '<strong class="red">' . __('No', true) . '</strong>'
                    )
                )
              ),
              "displayedAs" => array(
                'letter_date' => 'date',
                'created' => 'datetime'
              )
        ));
?>
</div>
<?php if ($profile['group_id'] == 1):;?>
<?php echo $javascript->codeBlock($varJS);?>
<script type="text/javascript">
    $(function() {
        $('.getRecord').each(function() {
            var __id = $(this).attr('href').substring(__pathBeforeId.length);
            $(this).attr('href', __pathReplacement + __id);
        });
    });
</script>
<?php endif;?>