<?php echo $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php echo $html->css('letter', 'stylesheet', array('inline' => false));?>
<?php echo $javascript->link('letter_outs');?>
<?php echo $javascript->link('jquery.colorbox-min', false);?>
<?php echo $javascript->link('jquery.carouFredSel-3.2.0', false);?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php
        echo $form->create('LetterOut', array(
            'enctype' => 'multipart/form-data'
        ));
    ?>
	<fieldset>
 		<legend><?php __('Bon Nomor Surat Keluar');?></legend>
        <table class="input">
            <tr>
                <td><?php echo __('No. Surat');?>:</td>
                <td>
                    <?php
                        echo $form->input('letter_no', array(
                                'div'=>false, 'label' => false, 'class'=>'required'
                            )
                        );
                    ?>
                </td>
            </tr>
            <tr>
                <td>Unit Kerja<br />penandatangan surat</td>
                <td>
                    <?php
                        // QUICK HACK
                        echo $form->input('pad', array('div'=>false, 'label' => false, 'value' => '1', 'type' => 'hidden'));
                    ?>
                    <?php
                        echo $form->select('unit_code_id', $unit_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('unit_code_id')) ? $form->error('unit_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Kode Hal');?>:</td>
                <td>
                    <input type="radio" name="cat_term" class="cat_term" value="Substantif" /> Substantif &nbsp;
                    <input type="radio" name="cat_term" class="cat_term" value="Fasilitatif" /> Fasilitatif &nbsp;<br />
                    <?php
                        echo $form->select('term_code_id', $term_codes, null);
                        echo ($form->isFieldError('term_code_id')) ? $form->error('term_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Hal');?>:</td>
                <td><?php echo $form->input('terms', array('div'=>false, 'label' => false, 'size' => 35));?></td>
            </tr>
            <tr>
                <td class="label-required">
                <?php echo __('Ditujukan kepada');?>:</td>
                <td>
                <?php
                    // quick hack, change to text
                    echo $form->input('letter_to', array('div'=>false, 'label' => false, 'class'=>'required', 'type' => 'textarea'));
                    ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Date');?>:</td>
                <td><?php echo $form->input('letter_date', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Ditandatangani oleh');?>:</td>
                <td><?php echo $form->input('remark', array('div'=>false, 'label' => false));?></td>
            </tr>
            <tr>
                <td class="label-required">Gambar</td>
                <td>
                <div class="carousel">
                <?php
                    if (!empty($letter_out['LetterOutImage']) ):
                        echo '<ul>';
                        foreach ( $letter_out['LetterOutImage'] as $cimg):
                            echo '<li><a href="'.$urlImage . $resizedPrefix . $cimg['imagename'] .
                                        '" class="colorbox" rel="' . $letter_out['LetterOut']['id'] . '">' .
                                 '<img src="' . $urlImage . $thumbPrefix . $cimg['imagename'] . '" ' .
                                 'width="25" height="25" /></a>' .
                                 $html->link('Hapus', array('action' => 'delImage', $cimg['id'], $letter_out['LetterOut']['id']),
                                    array('class' => 'delImage')
                                 ) . '</li>';
                        endforeach;
                        echo '</ul>';
                        echo '<div class="clearfix"></div>';
                        echo '<a id="prev" href="#">&lt;</a>';
                        echo '<a id="next" href="#">&gt;</a>';
                    endif;
                ?>
                </div>
                <div id="upload_image">
                    <?php
                        echo $form->input('imagename', array(
                            'div'=>false, 'label'=>false, 'class'=>'imagename', 'id' => false,
                            'name' => 'data[LetterOut][LetterOutImage][imagename][]', 'type' => 'file'
                        ));
                    ?>
                </div>
                <a href="#" id="add_upload_image">Tambah gambar</a>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Komitmen');?>:</td>
                <td>
                <?php
                    // quick hack, only js
                    echo $form->input('komitmen', array('div'=>false, 'label' => false, 'type' => 'checkbox', 'checked' => true));
                ?>
                <p>Saya akan menyimpan arsip surat ini, dan jika diminta atau tidak diminta oleh bagian Administrasi Umum dan Keuangan,
saya akan menyerahkan arsip surat ini ke Penata Arsip di Bagian Administrasi Umum dan Keuangan Politeknik Negeri Media Kreatif.</p>
                </td>
            </tr>
            <?php if (isset($this->data['LetterOut']['verified']) && $this->data['LetterOut']['verified']):?>
            <tr>
                <td colspan="2"><strong>Sudah diverifikasi :</strong></td>
            </tr>
            <tr>
                <td>No Lemari</td>
                <td><strong>
                <?php echo $this->data['LetterOut']['cabinet_no'];?></strong>
                </td>
            </tr>
            <tr>
                <td>No Box</td>
                <td><strong>
                <?php echo $this->data['LetterOut']['box_no'];?></strong>
                </td>
            </tr>
            <tr>
                <td>Jadwal retensi / status arsip</td>
                <td><strong>
                <?php
                    $_retention = array(
                        1 => 'Inaktif 1 tahun',
                        2 => 'Inaktif 2 tahun',
                        3 => 'Inaktif 3 tahun',
                        4 => 'Inaktif 4 tahun',
                        5 => 'Inaktif 5 tahun',
                        6  => 'Inaktif 6 tahun',
                        7  => 'Inaktif 7 tahun',
                        8  => 'Inaktif 8 tahun',
                        9  => 'Inaktif 9 tahun',
                        10 => 'Inaktif 10 tahun',
                        15 => 'Inaktif 15 tahun',
                        0 => 'Musnah',
                        99 => 'Permanen',
                    );
                    echo isset($_retention[$this->data['LetterOut']['retention']]) ?
                         $_retention[$this->data['LetterOut']['retention']] : '';
                ?></strong>
                </td>
            </tr>
            <?php endif;?>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Simpan', true), array('div'=>false, 'id' => 'request_no')) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['LetterOut']['id']), array('class'=>'del'), sprintf(__('Apakah Anda yakin ingin menghapus', true) . ' %s?', ' no ' . $this->data['LetterOut']['letter_no'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<script type="text/javascript">
var substantif = <?php echo $substantif;?>;
var substantifHtml = '<option value=""></option>';
var fasilitatif = <?php echo $fasilitatif;?>;
var fasilitatifHtml = '<option value=""></option>';
for (key in substantif) {
    substantifHtml += '<option value="' + key + '">' + substantif[key] + '</option>';
}
for (key in fasilitatif) {
    fasilitatifHtml += '<option value="' + key + '">' + fasilitatif[key] + '</option>';
}
$(function() {
    $('.cat_term').click(function() {
        if ( this.value == 'Substantif' ) {
            $('#LetterOutTermCodeId').html(substantifHtml);
        } else {
            $('#LetterOutTermCodeId').html(fasilitatifHtml);
        }
    });

    var current = $('#LetterOutTermCodeId').val();
    if ( array_key_exists( current, substantif) ) {
        $('#LetterOutTermCodeId').html(substantifHtml);
        $('.cat_term[value="Substantif"]').attr('checked', 'checked');
        $('#LetterOutTermCodeId option[value="'+current+'"]').attr('selected', 'selected');
    } else if ( array_key_exists( current, fasilitatif) ) {
        $('#LetterOutTermCodeId').html(fasilitatifHtml);
        $('.cat_term[value="Fasilitatif"]').attr('checked', 'checked');
        $('#LetterOutTermCodeId option[value="'+current+'"]').attr('selected', 'selected');
    }

    $('#add_upload_image').click(function(e) {
        var cloned = $('.imagename:last', '#upload_image').clone();
        cloned.val('');
        $('#upload_image').append('<br />');
        $('#upload_image').append(cloned);

        e.preventDefault();
        return false;
    });

    $('.delImage').click(function(e) {
        if ( confirm('Yakin ingin menghapus gambar ini?') ) {
            return true;
        }
        e.preventDefault();
        return false;
    });

    $('.colorbox').colorbox({
        close: "Tutup"
    });

    <?php if (!empty($letter_out['LetterOutImage']) ):?>
    $('.carousel ul').carouFredSel({
        circular: false,
        infinite: false,
        auto: false,
        prev: "#prev",
        next: "#next"
    });
    <?php endif;?>
});

function array_key_exists( key, search ) {
    // Checks if the given key or index exists in the array
    //
    // version: 1009.2513
    // discuss at: http://phpjs.org/functions/array_key_exists    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Felix Geisendoerfer (http://www.debuggable.com/felix)
    // *     example 1: array_key_exists('kevin', {'kevin': 'van Zonneveld'});
    // *     returns 1: true
    // input sanitation
    if (!search || (search.constructor !== Array && search.constructor !== Object)){
        return false;
    }

    return key in search;
}
</script>
