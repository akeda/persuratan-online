<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid.verify',
        array("fields" => array(
                "letter_no" => array(
                    'title' => __("Letter No.", true), 
                    'sortable' => false
                ),
                "letter_date" => array(
                    'title' => __("Date", true),
                    'sortable' => true
                ),
                "created_by" => array(
                    'title' => __("Di input oleh", true),
                    'sortable' => true
                ),
                "unit_code_id" => array(
                    'title' => __("Unit", true),
                    'sortable' => true
                ),
                "term_code_id" => array(
                    'title' => __("Kode Hal", true),
                    'sortable' => true
                ),
                "terms" => array(
                    'title' => __("Hal", true),
                    'sortable' => false
                ),
                "letter_to" => array(
                    'title' => __("Ditujukan kepada", true),
                    'sortable' => false
                ),
                "remark" => array(
                    'title' => __("Ditandatangani oleh", true),
                    'sortable' => false
                ),
                "created" => array(
                    'title' => __("Waktu input", true),
                    'sortable' => true
                ),
                'verified' => array(
                    'title' => __("Sudah diverifikasi", true),
                    'sortable' => false
                )
              ),
              "editable"  => "letter_no",
              "editableLink"  => "verify",
              "assoc" => array(
                'unit_code_id' => array(
                    'field' => 'name',
                    'model' => 'UnitCode'
                ),
                'term_code_id' => array(
                    'field' => 'name',
                    'model' => 'TermCode'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              "replacement" => array(
                'active' => array(
                    1 => array(
                        'replaced' => '<strong class="green">' . __('Yes', true) . '</strong>'
                    ),
                    0 => array(
                        'replaced' => '<strong class="red">' . __('No', true) . '</strong>'
                    )
                ),
                'verified' => array(
                    0 => array(
                        'replaced' => '<span class="red">Belum</span>'
                    ),
                    1 => array(
                        'replaced' => '<span class="green">Sudah</span>'
                    )
                )
              ),
              "displayedAs" => array(
                'letter_date' => 'date',
                'created' => 'datetime'
              )
        ));
?>
</div>
