<?php echo $html->css('letter', 'stylesheet', array('inline' => false));?>
<?php echo $javascript->link('letter_outs');?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php
        echo $form->create('LetterOut', array(
            'enctype' => 'multipart/form-data'
        ));
    ?>
	<fieldset>
 		<legend><?php __('Bon Nomor Surat Keluar');?></legend>
        <table class="input">
            <tr>
                <td colspan="2">
                    <?php
                        echo $form->input('letter_no', array(
                                'div'=>false, 'label' => false, 'maxlength' => 100,
                                'class'=>'required', 'value' => '', 'type' => 'hidden'
                            )
                        );
                        echo ($form->isFieldError('letter_no')) ? $form->error('letter_no') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td>Unit Kerja<br />penandatangan surat</td>
                <td>
                    <?php
                        // QUICK HACK
                        echo $form->input('pad', array('div'=>false, 'label' => false, 'value' => '1', 'type' => 'hidden'));
                    ?>
                    <?php
                        echo $form->select('unit_code_id', $unit_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('unit_code_id')) ? $form->error('unit_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td>Kode Hal:</td>
                <td>
                    <input type="radio" name="cat_term" class="cat_term" value="Substantif" /> Substantif &nbsp;
                    <input type="radio" name="cat_term" class="cat_term" value="Fasilitatif" /> Fasilitatif &nbsp;<br />
                    <?php
                        echo $form->select('term_code_id', $term_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('term_code_id')) ? $form->error('term_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Hal');?>:</td>
                <td><?php echo $form->input('terms', array('div'=>false, 'label' => false, 'size' => 35));?></td>
            </tr>
            <tr>
                <td class="label-required">
                <?php echo __('Ditujukan kepada');?>:</td>
                <td>
                <?php
                    // quick hack, change to text
                    echo $form->input('letter_to', array('div'=>false, 'label' => false, 'class'=>'required', 'type' => 'textarea'));
                    ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Date');?>:</td>
                <td><?php echo $form->input('letter_date', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Ditandatangani oleh');?>:</td>
                <td><?php echo $form->input('remark', array('div'=>false, 'label' => false));?></td>
            </tr>
            <tr>
                <td class="label-required">Gambar</td>
                <td>
                <div id="upload_image">
                    <?php
                        echo $form->input('imagename', array(
                            'div'=>false, 'label'=>false, 'class'=>'imagename', 'id' => false,
                            'name' => 'data[LetterOut][LetterOutImage][imagename][]', 'type' => 'file'
                        ));
                    ?>
                </div>
                <a href="#" id="add_upload_image">Tambah gambar</a>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Komitmen');?>:</td>
                <td>
                <?php
                    // quick hack, only js
                    echo $form->input('komitmen', array('div'=>false, 'label' => false, 'type' => 'checkbox'));
                ?>
                <p>Saya akan menyimpan arsip surat ini, dan jika diminta atau tidak diminta oleh bagian Administrasi Umum dan Keuangan,
saya akan menyerahkan arsip surat ini ke Penata Arsip di Bagian Administrasi Umum dan Keuangan Politeknik Negeri Media Kreatif.</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Minta No. Surat', true), array('div'=>false, 'id' => 'request_no')) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));

                    if ( isset($succeed) ) {
                        echo $succeed;
                    }
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<script type="text/javascript">
var substantif = <?php echo $substantif;?>;
var substantifHtml = '<option value=""></option>';
var fasilitatif = <?php echo $fasilitatif;?>;
var fasilitatifHtml = '<option value=""></option>';
for (key in substantif) {
    substantifHtml += '<option value="' + key + '">' + substantif[key] + '</option>';
}
for (key in fasilitatif) {
    fasilitatifHtml += '<option value="' + key + '">' + fasilitatif[key] + '</option>';
}
$(function() {
    $('.cat_term').click(function() {
        if ( this.value == 'Substantif' ) {
            $('#LetterOutTermCodeId').html(substantifHtml);
        } else {
            $('#LetterOutTermCodeId').html(fasilitatifHtml);
        }
    });

    $('.cat_term:first').trigger('click');

    $('#add_upload_image').click(function(e) {
        var cloned = $('.imagename:last', '#upload_image').clone();
        cloned.val('');
        $('#upload_image').append('<br />');
        $('#upload_image').append(cloned);

        e.preventDefault();
        return false;
    });
});
</script>
