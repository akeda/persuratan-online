<?php $javascript->link('jquery.validate.min', false); ?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('TermCode');?>
	<fieldset>
 		<legend><?php __('Add Term Code');?></legend>
        <table class="input">
            <tr>
                <td class="label-required">Deskripsi Hal:</td>
                <td><?php echo $form->input('name', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Kode Hal:</td>
                <td><?php echo $form->input('code', array('div'=>false, 'label' => false, 'maxlength' => 20, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required">Kategori:</td>
                <td>
                <?php
                    echo $form->select('category',
                        array('Substantif' => 'Substantif', 'Fasilitatif' => 'Fasilitatif'), null,
                        array('div'=>false, 'label' => false)
                    );
                    echo ($form->isFieldError('category')) ? $form->error('category') : '';
                ?>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
