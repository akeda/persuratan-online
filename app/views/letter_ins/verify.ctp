<?php echo $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php echo $html->css('letter', 'stylesheet', array('inline' => false));?>
<?php echo $javascript->link('jquery.colorbox-min', false);?>
<?php echo $javascript->link('jquery.carouFredSel-3.2.0', false);?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php
        echo $form->create('LetterIn', array(
            'enctype' => 'multipart/form-data',
            'action'=>'verify'
        ));
    ?>
	<fieldset>
 		<legend>
        <?php
            echo $arsip ? 'Verifikasi Arsip' : 'Verifikasi Surat Masuk';
        ?>
        </legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Letter No.');?>:</td>
                <td>
                <?php
                    echo $letter['LetterIn']['letter_no'];
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('From');?>:</td>
                <td>
                <?php
                    echo $letter['LetterIn']['letter_from'];
                ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Date');?>:</td>
                <td>
                <?php
                    echo $time->format('d/m/Y', $letter['LetterIn']['letter_date']);
                ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Tgl. Terima Surat');?>:</td>
                <td>
                <?php
                    echo $time->format('d/m/Y', $letter['LetterIn']['received_date']);
                ?>
                </td>
            </tr>
            <!-- ubah alamat ke field type text -->
            <tr>
                <td><?php echo __('Alamat');?>:</td>
                <td>
                    <?php
                        echo $letter['LetterIn']['letter_from_address'];
                    ?>
                </td>
            </tr>
            <tr>
                <td>Klasifikasi Arsip (HAL):</td>
                <td>
                    <input type="radio" name="cat_term" class="cat_term" value="Substantif" /> Substantif &nbsp;
                    <input type="radio" name="cat_term" class="cat_term" value="Fasilitatif" /> Fasilitatif &nbsp;<br />
                    <?php
                        echo $form->select('term_code_id', $term_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('term_code_id')) ? $form->error('term_code_id') : '';
                    ?>
                </td>
            </tr>
            <!-- ubah hal ke field type text -->
            <tr>
                <td><?php echo __('Hal');?>:</td>
                <td>
                <?php
                    echo $letter['LetterIn']['letter_term'];
                ?>
                </td>
            </tr>
            <!-- perlu di tambahkan di database tanggal disposisi -->
            <tr>
                <td><?php echo __('Tanggal Disposisi');?>:</td>
                <td>
                <?php
                    echo $time->format('d/m/Y', $letter['LetterIn']['disposition_date']);
                ?>
                </td>
            </tr>
            <!-- ubah field type ke text untuk letter_to -->
            <tr>
                <td><?php echo __('Di disposisi kepada');?>:</td>
                <td>
                <?php
                    echo $letter['LetterIn']['letter_to'];
                ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Unit');?>:</td>
                <td>
                    <?php
                        echo $form->select('unit_code_id', $unit_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('unit_code_id')) ? $form->error('unit_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gambar</td>
                <td>
                <div class="carousel">
                <?php
                    if (!empty($letter_in['LetterInImage']) ):
                        echo '<ul>';
                        foreach ( $letter_in['LetterInImage'] as $cimg):
                            echo '<li><a href="'.$urlImage . $resizedPrefix . $cimg['imagename'] .
                                        '" class="colorbox" rel="' . $letter_in['LetterIn']['id'] . '">' .
                                 '<img src="' . $urlImage . $thumbPrefix . $cimg['imagename'] . '" ' .
                                 'width="25" height="25" /></a>' .
                                 $html->link('Hapus', array('action' => 'delImage', $cimg['id'], $letter_in['LetterIn']['id'] . '?verify'),
                                    array('class' => 'delImage')
                                 ) . '</li>';
                        endforeach;
                        echo '</ul>';
                        echo '<div class="clearfix"></div>';
                        echo '<a id="prev" href="#">&lt;</a>';
                        echo '<a id="next" href="#">&gt;</a>';
                    endif;
                ?>
                </div>
                <div id="upload_image">
                    <?php
                        echo $form->input('imagename', array(
                            'div'=>false, 'label'=>false, 'class'=>'imagename', 'id' => false,
                            'name' => 'data[LetterIn][LetterInImage][imagename][]', 'type' => 'file'
                        ));
                    ?>
                </div>
                <a href="#" id="add_upload_image">Tambah gambar</a>
                </td>
            </tr>
            <tr>
                <td>
                No Lemari<br />
                <span class="label">No Lemari diisi jika arsip ditempatkan di lemari</span>
                </td>
                <td>
                <?php
                    echo $form->input('cabinet_no', array(
                        'div'=>false, 'label' => false, 'class' => 'cabinet'
                    ));
                    ?>
                </td>
            </tr>
            <tr>
                <td>
                No Box<br />
                <span class="label">No Box diisi jika arsip ditempatkan di lemari</span>
                </td>
                <td>
                <?php
                    echo $form->input('box_no', array(
                        'div'=>false, 'label' => false, 'class' => 'cabinet'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td>
                No Filling cabinet<br />
                <span class="label">No Filling cabinet diisi jika arsip ditempatkan di cabinet</span>
                </td>
                <td>
                <?php
                    echo $form->input('filling_cabinet_no', array(
                        'div'=>false, 'label' => false, 'class' => 'filling_cabinet'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td>
                No Laci<br />
                <span class="label">No Laci diisi jika arsip ditempatkan di cabinet</span>
                </td>
                <td>
                <?php
                    echo $form->input('filling_box_no', array(
                        'div'=>false, 'label' => false, 'class' => 'filling_cabinet'
                    ));
                ?>
                </td>
            </tr>
            <tr>
                <td>Jadwal retensi / status arsip</td>
                <td>
                <?php
                    echo $form->select('retention',
                        array(
                            1  => 'Inaktif 1 tahun',
                            2  => 'Inaktif 2 tahun',
                            3  => 'Inaktif 3 tahun',
                            4  => 'Inaktif 4 tahun',
                            5  => 'Inaktif 5 tahun',
                            6  => 'Inaktif 6 tahun',
                            7  => 'Inaktif 7 tahun',
                            8  => 'Inaktif 8 tahun',
                            9  => 'Inaktif 9 tahun',
                            10 => 'Inaktif 10 tahun',
                            15 => 'Inaktif 15 tahun',
                            0  => 'Musnah',
                            99 => 'Permanen',
                        ),
                        null,
                        array('div'=>false, 'label' => false)
                    );
                ?>
                </td>
            </tr>
            <tr>
                <td>Verifikasi:</td>
                <td>
                <?php
                    echo $form->input('verified', array('div'=>false, 'label' => false, 'type' => 'checkbox', 'checked' => true));
                ?>
                <p>Saya sudah memverifikasi surat ini, dan jika diminta atau tidak diminta oleh bagian Administrasi Umum dan Keuangan,
saya akan menyerahkan arsip surat ini ke Penata Arsip di Bagian Administrasi Umum dan Keuangan Politeknik Negeri Media Kreatif.</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit('Verifikasi', array('div' => false, 'id' => 'verify')) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['LetterIn']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', ' no ' . $this->data['LetterIn']['letter_no'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'verify_list'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<script type="text/javascript">
var substantif = <?php echo $substantif;?>;
var substantifHtml = '<option value=""></option>';
var fasilitatif = <?php echo $fasilitatif;?>;
var fasilitatifHtml = '<option value=""></option>';
for (key in substantif) {
    substantifHtml += '<option value="' + key + '">' + substantif[key] + '</option>';
}
for (key in fasilitatif) {
    fasilitatifHtml += '<option value="' + key + '">' + fasilitatif[key] + '</option>';
}
$(function() {
    $('.cat_term').click(function() {
        if ( this.value == 'Substantif' ) {
            $('#LetterInTermCodeId').html(substantifHtml);
        } else {
            $('#LetterInTermCodeId').html(fasilitatifHtml);
        }
    });

    $('#verify').click(function(e) {
        var no_lemari = $('#LetterInCabinetNo').val();
        var no_box = $('#LetterInBoxNo').val();
        var no_cabinet = $('#LetterInFillingCabinetNo').val();
        var no_laci = $('#LetterInFillingBoxNo').val();

        if ( (no_lemari.length && no_box.length) || (no_cabinet.length && no_laci.length ) ) {
            return true;
        } else {
            alert('No Lemari/Cabinet dan No Box/Laci harus diisi');
            e.preventDefault();
            return false;
        }
    });

    var current = $('#LetterInTermCodeId').val();
    if ( array_key_exists( current, substantif) ) {
        $('.cat_term[value="Substantif"]').trigger('click');
        $('#LetterInTermCodeId').val(current);
    } else if ( array_key_exists( current, fasilitatif) ) {
        $('.cat_term[value="Fasilitatif"]').trigger('click');
        $('#LetterInTermCodeId').val(current);
    }

    // choose between ordinary cabinet or filling cabinet
    $('.cabinet').blur(function() {
        if ( this.value.length ) {
            $('.filling_cabinet').val('');
        }
    });
    $('.filling_cabinet').blur(function() {
        if ( this.value.length ) {
            $('.cabinet').val('');
        }
    });

    $('#add_upload_image').click(function(e) {
        var cloned = $('.imagename:last', '#upload_image').clone();
        cloned.val('');
        $('#upload_image').append('<br />');
        $('#upload_image').append(cloned);

        e.preventDefault();
        return false;
    });

    $('.delImage').click(function(e) {
        if ( confirm('Yakin ingin menghapus gambar ini?') ) {
            return true;
        }
        e.preventDefault();
        return false;
    });

    $('.colorbox').colorbox({
        close: "Tutup"
    });

    <?php if (!empty($letter_in['LetterInImage']) ):?>
    $('.carousel ul').carouFredSel({
        circular: false,
        infinite: false,
        auto: false,
        prev: "#prev",
        next: "#next"
    });
    <?php endif;?>
});

function array_key_exists( key, search ) {
    // Checks if the given key or index exists in the array
    //
    // version: 1009.2513
    // discuss at: http://phpjs.org/functions/array_key_exists    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Felix Geisendoerfer (http://www.debuggable.com/felix)
    // *     example 1: array_key_exists('kevin', {'kevin': 'van Zonneveld'});
    // *     returns 1: true
    // input sanitation
    if (!search || (search.constructor !== Array && search.constructor !== Object)){
        return false;
    }

    return key in search;
}
</script>
