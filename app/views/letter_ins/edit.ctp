<?php echo $html->css('colorbox', 'stylesheet', array('inline' => false));?>
<?php echo $html->css('letter', 'stylesheet', array('inline' => false));?>
<?php echo $javascript->link('jquery.colorbox-min', false);?>
<?php echo $javascript->link('jquery.carouFredSel-3.2.0', false);?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php
        echo $form->create('LetterIn', array(
            'enctype' => 'multipart/form-data'
        ));
    ?>
	<fieldset>
 		<legend><?php __('Edit Letter In');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Letter No.');?>:</td>
                <td><?php echo $form->input('letter_no', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('From');?>:</td>
                <td><?php echo $form->input('letter_from', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Date');?>:</td>
                <td><?php echo $form->input('letter_date', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Tgl. Terima Surat');?>:</td>
                <td><?php echo $form->input('received_date', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <!-- ubah alamat ke field type text -->
            <tr>
                <td><?php echo __('Alamat');?>:</td>
                <td>
                    <?php
                        echo $form->input('letter_from_address', array('div'=>false, 'label' => false, 'type' => 'textarea', 'rows' => 3));
                    ?>
                </td>
            </tr>
            <tr>
                <td>Klasifikasi Arsil (HAL):</td>
                <td>
                    <input type="radio" name="cat_term" class="cat_term" value="Substantif" /> Substantif &nbsp;
                    <input type="radio" name="cat_term" class="cat_term" value="Fasilitatif" /> Fasilitatif &nbsp;<br />
                    <?php
                        echo $form->select('term_code_id', $term_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('term_code_id')) ? $form->error('term_code_id') : '';
                    ?>
                </td>
            </tr>
            <!-- ubah hal ke field type text -->
            <tr>
                <td><?php echo __('Hal');?>:</td>
                <td><?php echo $form->input('letter_term', array('div'=>false, 'label' => false, 'type' => 'textarea', 'rows' => 3));?></td>
            </tr>
            <!-- perlu di tambahkan di database tanggal disposisi -->
            <tr>
                <td><?php echo __('Tanggal Disposisi');?>:</td>
                <td><?php echo $form->input('disposition_date', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <!-- ubah field type ke text untuk letter_to -->
            <tr>
                <td><?php echo __('Di disposisi kepada');?>:</td>
                <td><?php echo $form->input('letter_to', array('div'=>false, 'label' => false, 'type' => 'textarea', 'rows' => 3));?></td>
            </tr>
            <tr>
                <td><?php echo __('Unit');?>:</td>
                <td>
                    <?php
                        echo $form->select('unit_code_id', $unit_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('unit_code_id')) ? $form->error('unit_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gambar</td>
                <td>
                <div class="carousel">
                <?php
                    if (!empty($letter_in['LetterInImage']) ):
                        echo '<ul>';
                        foreach ( $letter_in['LetterInImage'] as $cimg):
                            echo '<li><a href="'.$urlImage . $resizedPrefix . $cimg['imagename'] .
                                        '" class="colorbox" rel="' . $letter_in['LetterIn']['id'] . '">' .
                                 '<img src="' . $urlImage . $thumbPrefix . $cimg['imagename'] . '" ' .
                                 'width="25" height="25" /></a>' .
                                 $html->link('Hapus', array('action' => 'delImage', $cimg['id'], $letter_in['LetterIn']['id']),
                                    array('class' => 'delImage')
                                 ) . '</li>';
                        endforeach;
                        echo '</ul>';
                        echo '<div class="clearfix"></div>';
                        echo '<a id="prev" href="#">&lt;</a>';
                        echo '<a id="next" href="#">&gt;</a>';
                    endif;
                ?>
                </div>
                <div id="upload_image">
                    <?php
                        echo $form->input('imagename', array(
                            'div'=>false, 'label'=>false, 'class'=>'imagename', 'id' => false,
                            'name' => 'data[LetterIn][LetterInImage][imagename][]', 'type' => 'file'
                        ));
                    ?>
                </div>
                <a href="#" id="add_upload_image">Tambah gambar</a>
                </td>
            </tr>
            <?php if (isset($this->data['LetterIn']['verified']) && $this->data['LetterIn']['verified']):?>
            <tr>
                <td colspan="2"><strong>Sudah diverifikasi :</strong></td>
            </tr>
            <tr>
                <td>No Lemari</td>
                <td><strong>
                <?php echo $this->data['LetterIn']['cabinet_no'];?></strong>
                </td>
            </tr>
            <tr>
                <td>No Box</td>
                <td><strong>
                <?php echo $this->data['LetterIn']['box_no'];?></strong>
                </td>
            </tr>
            <tr>
                <td>Jadwal retensi / status arsip</td>
                <td><strong>
                <?php
                    $_retention = array(
                        1 => 'Inaktif 1 tahun',
                        2 => 'Inaktif 2 tahun',
                        3 => 'Inaktif 3 tahun',
                        4 => 'Inaktif 4 tahun',
                        5 => 'Inaktif 5 tahun',
                        6  => 'Inaktif 6 tahun',
                        7  => 'Inaktif 7 tahun',
                        8  => 'Inaktif 8 tahun',
                        9  => 'Inaktif 9 tahun',
                        10 => 'Inaktif 10 tahun',
                        15 => 'Inaktif 15 tahun',
                        0 => 'Musnah',
                        99 => 'Permanen',
                    );
                    echo isset($_retention[$this->data['LetterIn']['retention']]) ?
                         $_retention[$this->data['LetterIn']['retention']] : '';
                ?></strong>
                </td>
            </tr>
            <?php endif;?>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Save', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['LetterIn']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', ' no ' . $this->data['LetterIn']['letter_no'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<script type="text/javascript">
var substantif = <?php echo $substantif;?>;
var substantifHtml = '<option value=""></option>';
var fasilitatif = <?php echo $fasilitatif;?>;
var fasilitatifHtml = '<option value=""></option>';
for (key in substantif) {
    substantifHtml += '<option value="' + key + '">' + substantif[key] + '</option>';
}
for (key in fasilitatif) {
    fasilitatifHtml += '<option value="' + key + '">' + fasilitatif[key] + '</option>';
}
$(function() {
    $('.cat_term').click(function() {
        if ( this.value == 'Substantif' ) {
            $('#LetterInTermCodeId').html(substantifHtml);
        } else {
            $('#LetterInTermCodeId').html(fasilitatifHtml);
        }
    });

    var current = $('#LetterInTermCodeId').val();
    if ( array_key_exists( current, substantif) ) {
        $('.cat_term[value="Substantif"]').trigger('click');
        $('#LetterInTermCodeId').val(current);
    } else if ( array_key_exists( current, fasilitatif) ) {
        $('.cat_term[value="Fasilitatif"]').trigger('click');
        $('#LetterInTermCodeId').val(current);
    }

    $('#add_upload_image').click(function(e) {
        var cloned = $('.imagename:last', '#upload_image').clone();
        cloned.val('');
        $('#upload_image').append('<br />');
        $('#upload_image').append(cloned);

        e.preventDefault();
        return false;
    });

    $('.delImage').click(function(e) {
        if ( confirm('Yakin ingin menghapus gambar ini?') ) {
            return true;
        }
        e.preventDefault();
        return false;
    });

    $('.colorbox').colorbox({
        close: "Tutup"
    });

    <?php if (!empty($letter_in['LetterInImage']) ):?>
    $('.carousel ul').carouFredSel({
        circular: false,
        infinite: false,
        auto: false,
        prev: "#prev",
        next: "#next"
    });
    <?php endif;?>
});

function array_key_exists( key, search ) {
    // Checks if the given key or index exists in the array
    //
    // version: 1009.2513
    // discuss at: http://phpjs.org/functions/array_key_exists    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +   improved by: Felix Geisendoerfer (http://www.debuggable.com/felix)
    // *     example 1: array_key_exists('kevin', {'kevin': 'van Zonneveld'});
    // *     returns 1: true
    // input sanitation
    if (!search || (search.constructor !== Array && search.constructor !== Object)){
        return false;
    }

    return key in search;
}
</script>
