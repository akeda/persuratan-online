<?php echo $html->css('letter', 'stylesheet', array('inline' => false));?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php
        echo $form->create('LetterIn', array(
            'enctype' => 'multipart/form-data'
        ));
    ?>
	<fieldset>
 		<legend><?php __('Add Letter In');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('Letter No.');?>:</td>
                <td><?php echo $form->input('letter_no', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('From');?>:</td>
                <td><?php echo $form->input('letter_from', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Date');?>:</td>
                <td><?php echo $form->input('letter_date', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Tgl. Terima Surat');?>:</td>
                <td><?php echo $form->input('received_date', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <!-- ubah alamat ke field type text -->
            <tr>
                <td><?php echo __('Alamat');?>:</td>
                <td><?php echo $form->input('letter_from_address', array('div'=>false, 'label' => false, 'type' => 'textarea', 'rows' => 3));?></td>
            </tr>
            <tr>
                <td>Klasifikasi Arsil (HAL):</td>
                <td>
                    <input type="radio" name="cat_term" class="cat_term" value="Substantif" /> Substantif &nbsp;
                    <input type="radio" name="cat_term" class="cat_term" value="Fasilitatif" /> Fasilitatif &nbsp;<br />
                    <?php
                        echo $form->select('term_code_id', $term_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('term_code_id')) ? $form->error('term_code_id') : '';
                    ?>
                </td>
            </tr>
            <!-- ubah hal ke field type text -->
            <tr>
                <td><?php echo __('Hal');?>:</td>
                <td><?php echo $form->input('letter_term', array('div'=>false, 'label' => false, 'type' => 'textarea', 'rows' => 3));?></td>
            </tr>
            <!-- perlu di tambahkan di database tanggal disposisi -->
            <tr>
                <td><?php echo __('Tanggal Disposisi');?>:</td>
                <td><?php echo $form->input('disposition_date', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <!-- ubah field type ke text untuk letter_to -->
            <tr>
                <td><?php echo __('Di disposisi kepada');?>:</td>
                <td><?php echo $form->input('letter_to', array('div'=>false, 'label' => false, 'type' => 'textarea', 'rows' => 3));?></td>
            </tr>
            <tr>
                <td><?php echo __('Unit');?>:</td>
                <td>
                    <?php
                        echo $form->select('unit_code_id', $unit_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('unit_code_id')) ? $form->error('unit_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label-required">Gambar</td>
                <td>
                <div id="upload_image">
                    <?php
                        echo $form->input('imagename', array(
                            'div'=>false, 'label'=>false, 'class'=>'imagename', 'id' => false,
                            'name' => 'data[LetterIn][LetterInImage][imagename][]', 'type' => 'file'
                        ));
                    ?>
                </div>
                <a href="#" id="add_upload_image">Tambah gambar</a>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<script type="text/javascript">
var substantif = <?php echo $substantif;?>;
var substantifHtml = '<option value=""></option>';
var fasilitatif = <?php echo $fasilitatif;?>;
var fasilitatifHtml = '<option value=""></option>';
for (key in substantif) {
    substantifHtml += '<option value="' + key + '">' + substantif[key] + '</option>';
}
for (key in fasilitatif) {
    fasilitatifHtml += '<option value="' + key + '">' + fasilitatif[key] + '</option>';
}
$(function() {
    $('.cat_term').click(function() {
        if ( this.value == 'Substantif' ) {
            $('#LetterInTermCodeId').html(substantifHtml);
        } else {
            $('#LetterInTermCodeId').html(fasilitatifHtml);
        }
    });

    $('.cat_term:first').trigger('click');

    $('#add_upload_image').click(function(e) {
        var cloned = $('.imagename:last', '#upload_image').clone();
        cloned.val('');
        $('#upload_image').append('<br />');
        $('#upload_image').append(cloned);

        e.preventDefault();
        return false;
    });
});
</script>
