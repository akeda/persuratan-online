<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid.verify',
        array("fields" => array(
                "letter_no" => array(
                    'title' => __("Letter No.", true), 
                    'sortable' => false
                ),
                "received_date" => array(
                    'title' => __("Tgl. terima surat", true), 
                    'sortable' => true
                ),
                "letter_from" => array(
                    'title' => __("From", true),
                    'sortable' => false
                ),
                "letter_date" => array(
                    'title' => __("Date", true),
                    'sortable' => true
                ),
                "letter_from_address" => array(
                    // quick hack
                    'title' => __("Alamat", true),
                    'sortable' => false
                ),
                "letter_term" => array(
                    // quick hack
                    'title' => __("Hal", true),
                    'sortable' => false
                ),
                "letter_to" => array(
                    // quick hack
                    'title' => __("Di disposisi kepada", true),
                    'sortable' => false
                ),
                // tambahkan tanggal disposisi
                "disposition_date" => array(
                    'title' => __("Tgl. disposisi", true),
                    'sortable' => true
                ),
                "unit_code_id" => array(
                    'title' => __("Unit", true),
                    'sortable' => false
                ),
                "created_by" => array(
                // quick hack
                    'title' => __("Di input oleh", true),
                    'sortable' => true
                ),
                "created" => array(
                    'title' => __("Tgl. input", true),
                    'sortable' => true
                ),
                'verified' => array(
                    'title' => __("Sudah diverifikasi", true),
                    'sortable' => false
                )
              ),
              "editable"  => "letter_no",
              'editableLink' => 'verify',
              "assoc" => array(
                'unit_code_id' => array(
                    'field' => 'name',
                    'model' => 'UnitCode'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              "displayedAs" => array(
                'received_date' => 'date',
                'letter_date' => 'date',
                'created' => 'datetime'
              ),
              "replacement" => array(
                'received_date' => array(
                    '01/01/1970' => array(
                        'replaced' => ''
                    ),
                    '' => array(
                        'replaced' => ''
                    )
                ),
                'verified' => array(
                    0 => array(
                        'replaced' => '<span class="red">Belum</span>'
                    ),
                    1 => array(
                        'replaced' => '<span class="green">Sudah</span>'
                    )
                )
              ),
        ));
?>
</div>
