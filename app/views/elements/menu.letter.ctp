<?php 
    $menus = $this->requestAction(
        array(
            'controller' => 'menus', 'action' => 'menu'
        ), 
        array('pass' => array('letters'))
    );
?>
<div class="sidebar">
    <div class="sidebarc">
        <div class="sidebarch">
            <h2><?php echo $menus['name'];?></h2>
        </div>
        <div class="sidebarcb">
            <?php
                $parentStack = array();
                echo '<ul class="sidebar_menus">';
                foreach ($menus['lists'] as $menu) {
                    if ( $menu['Menu']['parent_id'] ) {
                        // if current menus's parent_id not yet pushed into $parentStack
                        if ( !array_key_exists($menu['Menu']['parent_id'], $parentStack) ) {
                            $parentStack[$menu['Menu']['parent_id']] = $menu['Menu']['parent_id'];
                            echo '<ul class="menus_parent">';
                        }
                        echo '<li id="' . $menu['Menu']['element_id'] . '"><a href="' . $menu['Menu']['url'] . '"><strong><strong><strong>' . $menu['Menu']['name'] . '</strong></strong></a></strong></li>';
                    } else {
                        if ( !empty($parentStack) ) {
                            echo '</ul>';
                        }
                        
                        echo '<li id="' . $menu['Menu']['element_id'] . '">';
                        // if has child don't print anchor
                        if ( $menu['Menu']['hasChild'] ) {
                            echo '<strong><strong><strong>' . $menu['Menu']['name'] . '</strong></strong></strong>';
                        } else {
                            echo $html->link('<strong><strong><strong>' . $menu['Menu']['name'] . '</strong></strong></strong>',
                                $menu['Menu']['url'], null, false, false);
                        }
                        echo '</li>';
                    }
                    
                }
                echo '</ul>';
            ?>
        </div>
    </div>
</div>