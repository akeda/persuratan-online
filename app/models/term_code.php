<?php
class TermCode extends AppModel {
    var $validate = array(
        'name' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => '/^\w+[\w\s]?/',
                'message' => 'This field cannot be left blank and must be alphanumeric'
            ),
            'maxlength' => array(
                'rule' => array('maxLength', 100),
                'message' => 'Maximum character is 100 characters'
            )
        ),
        'code' => array(
            'required' => array(    
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'notEmpty',
                'message' => 'This field cannot be left blank'
            ),
            'maxlength' => array(
                'rule' => array('maxLength', 20),
                'message' => 'Max character is 20 characters'
            )
        ),
        'category' => array(
            'required' => array(    
                'required' => true,
                'allowEmpty' => false,
                'rule' => 'alphanumeric',
                'message' => 'This field cannot be left blank and must be alphanumeric'
            ),
            'valid' => array(
                'rule' => array('inList', array('Fasilitatif', 'Substantif')),
                'message' => 'Pilih antara Fasilitatif atau Substantif'
            )
        )
    );
    
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by',
            'fields' => array('id', 'name')
        )
    );
}
?>
