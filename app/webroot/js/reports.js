$(function() {
    var oriAction = $('#reportForm').attr('action');
    var __m = oriAction.substring( oriAction.indexOf('?') );
    var pathPrint = path + '/printhtml' + __m;
    var pathGrafik = path + '/grafik' + __m;
    
    $('#by').change(function() {
        if ( this.value == 'p' ) {
            __path = path + '/getuser'; 
        } else if ( this.value == 'u' ) {
            __path = path + '/getunit';
        }
        
        if ( this.value.length ) {
            $.get(__path, function(tr) {
                $('#changed').html(tr);
            });
        } else {
            $('#changed').html('');
        }
    });
    
    $('#print').click(function(e) {
        $('#reportForm').attr('action', oriAction);
        
        if ( !$('#by').val().length ) {
            alert('Pilih per pengguna / unit kerja');
            e.preventDefault();
            return false
        }
    });
    
    $('#grafik').click(function(e) {
        $('#reportForm').attr('action', pathGrafik);
        
        if ( !$('#by').val().length ) {
            alert('Pilih per pengguna / unit kerja');
            e.preventDefault();
            return false
        }
    });
});